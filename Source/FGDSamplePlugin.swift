//
//  FGDSamplePlugin.swift
//  FGDSamplePlugin
//
//  Created by Gilberto De Faveri on 19/11/15.
//  Copyright © 2015 Foreground (omnidea srl). All rights reserved.
//
//  A Swift port with some minor improvements of RMSSamplePlugin, which is part
//  of Realmac Software's official RapidWeaver plugin SDK.
//
//  =============================================================================
//  This sofware is NOT ENDORSED NOR SUPPORTED by Realmac Software in any way.
//  =============================================================================

import Foundation

class FGDSamplePlugin: RWAbstractPlugin {
    
    var unloadPluginOnDocumentCloseCalled = false
    var contentViewController: FGDSamplePluginContentViewController?
    var optionsViewController: FGDSamplePluginOptionsViewController?
    
    dynamic var content = ""
    dynamic var emitRawContent = false
    dynamic var fileToken: String?
    
    //////////////////////////////////////
    // MARK: Protocol Methods
    //////////////////////////////////////
    
    override func optionsAndConfigurationView() -> (NSView?) {
        if optionsViewController == nil {
            optionsViewController = FGDSamplePluginOptionsViewController(representedObject: self)
        }
        return optionsViewController?.view
    }
    
    override func userInteractionAndEditingView() -> NSView! {
        if contentViewController == nil {
            contentViewController = FGDSamplePluginContentViewController(representedObject: self)
        }
        return contentViewController?.view
    }
    
    override func contentHTML(params: [NSObject : AnyObject]!) -> String! {
        
        let string = contentViewController!.content
        
        if emitRawContent == false {
            return string
        }
        
        // Original SamplePlugin cheats contentHTML here in order to return a dictionary
        // instead of a string. This is not allowed by Swift's stricter type check at runtime.
        return "Not supported 😭"
        
    }
    
    override func sidebarHTML(params: [NSObject : AnyObject]!) -> String! {
        return nil
    }
    
    override func normaliseImages() -> NSNumber! {
        return 0
    }
    
    func overrideFileExtension() -> NSString {
        return "htm"
    }
    
    func pluginWillAutoSave() {
        
    }
    
    func pluginDidAutoSave() {
        
    }
    
    //////////////////////////////////////
    // MARK: Object Lifecycle
    //////////////////////////////////////
    
    override init() {
        super.init()
        self.finishSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        content = aDecoder.decodeObjectForKey("Content String") as! String
        emitRawContent = aDecoder.decodeBoolForKey("Emit Raw Content")
        fileToken = aDecoder.decodeObjectForKey("File Token") as? String
        
        self.finishSetup()
    }
    
    override func encodeWithCoder(coder: NSCoder) {
        super.encodeWithCoder(coder)
        
        coder.encodeObject(contentViewController!.content, forKey: "Content String")
        coder.encodeBool(emitRawContent, forKey: "Emit Raw Content")
        coder.encodeObject(fileToken, forKey: "File Token")
        
    }
    
    deinit {
        
    }
    
    func unloadPluginOnDocumentClose() {        
        if (unloadPluginOnDocumentCloseCalled == false) {
            self.stopObservingVisibleKeys()
            
            // RW sometimes calls unloadPluginOnDocumentClose (where we call stopObservingVisibleKeys)
            // multiple times (e.g. when aborting project loading for missing theme).
            // This is why we need to set a variable in order to be sure we remove observers only once.
            unloadPluginOnDocumentCloseCalled = true
        }
    }
    
    func finishSetup() {
        
        self.observeVisibleKeys()
        
    }
    
    //////////////////////////////////////
    // MARK: KVO Broadcasting
    //////////////////////////////////////
    
    func visibleKeys() -> NSArray {
        return ["emitRawContent", "fileToken"];
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if self.visibleKeys().containsObject(keyPath!) {
            self.broadcastPluginChanged()
        }
    }
    
    func observeVisibleKeys() {
        for key in self.visibleKeys() {
            self.addObserver(self, forKeyPath: key as! String, options: [], context: nil)
        }
    }
    
    func stopObservingVisibleKeys() {
        for key in self.visibleKeys() {
            self.removeObserver(self, forKeyPath: key as! String)
        }
    }
    
    //////////////////////////////////////
    // MARK: Class Methods
    //////////////////////////////////////
    
    class func bundle() -> NSBundle {
        return NSBundle(forClass: FGDSamplePlugin.self)
    }
    
    override class func initializeClass(aBundle: NSBundle) -> Bool {
        return true
    }
    
    override class func pluginName() -> String {
        return NSLocalizedString("PluginName", tableName: nil, bundle: FGDSamplePlugin.bundle(), value: "Localizable", comment: "")
    }
    
    override class func pluginAuthor() -> String {
        return "My Company Name"
    }
    
    override class func pluginIcon() -> NSImage {
        let bundle = FGDSamplePlugin.bundle()
        let iconFilename = bundle.objectForInfoDictionaryKey("CFBundleIconFile")?.string
        return NSImage(contentsOfFile: bundle.pathForImageResource(iconFilename!)!)!
    }
    
    override class func pluginDescription() -> String {
        return NSLocalizedString("PluginDescription", tableName: nil, bundle: FGDSamplePlugin.bundle(), value: "Localizable", comment: "")
    }
    
}

