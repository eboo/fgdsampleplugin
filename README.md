# FGDSamplePlugin RapidWeaver Plugin #

A Swift port with some minor improvements of `RMSSamplePlugin`, which is part
of Realmac Software's official [RapidWeaver plugin SDK](https://github.com/realmacsoftware/RWPluginKit).

**This sofware is NOT ENDORSED NOR SUPPORTED by Realmac Software in any way.**